<?php
    /******* init.php ******/
    define("BASE_URL", 'http://lottery.iheard.me/iphone/');
    define("STATIC_URL", BASE_URL.'static/');
    define('ROOT_PATH', '/home/wwwroot/lottery.iheard.me/iphone/');
    define('SYSTEM_PATH', ROOT_PATH.'server/system/');
    define('VIEWS_PATH', ROOT_PATH.'server/views/');
    define('MODEL_PATH', ROOT_PATH.'server/model/');
    define('ACTION_PATH', ROOT_PATH.'server/action/');
    

    require_once(SYSTEM_PATH.'Base.action.php');
    require_once(SYSTEM_PATH.'Base.model.php');

    // 获取用户输入
    function _post($key){
        return isset($_POST[$key])?$_POST[$key]:'';
    }

    function _get($key){
        return isset($_GET[$key])?$_GET[$key]:'';
    }
    

    // 参数处理，加载对应C
    $c = _get('c');
    $m = _get('m');
    $d = './action/';
    require_once($d.$c.'.action.php'); 
    $C = new $c();
?>