<?php
    /**
    * 
    */
    class BaseAction
    {   
        private $smarty = '';

        function __construct()
        {
            require_once(SYSTEM_PATH.'smarty/Smarty.class.php');
            $this->smarty = new Smarty;

            $smarty->force_compile = true;
            $this->smarty->debugging = false;
            $this->smarty->caching = false;
            $this->smarty->cache_lifetime = 120;

            $this->smarty->left_delimiter = '{{'; 
            $this->smarty->right_delimiter = '}}'; 

            $this->smarty->setTemplateDir(VIEWS_PATH);
            $this->smarty->setCompileDir(SYSTEM_PATH.'smarty/templates_c/');
            $this->smarty->setConfigDir(SYSTEM_PATH.'smarty/configs/');
            $this->smarty->setCacheDir(SYSTEM_PATH.'smarty/cache/');

            $this->smarty->assign('BASE_URL',BASE_URL);
            $this->smarty->assign('STATIC_URL',STATIC_URL);


        }


        public function display($path){
            $this->smarty->display($path);
        }

        public function assign($key,$val){
            $this->smarty->assign($key, $val);
        }   
    }
?>