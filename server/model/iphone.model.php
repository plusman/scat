<?php
    /**
    * 
    */
    class iphoneModel extends BaseModel
    {
        
        function __construct()
        {
            parent::__construct();
        }


        function get_present_list($activity_code){
            $select = "iphone as tel,user_name as nick,'http://su.bdimg.com/static/superplus/img/logo_white.png' as url,id,company";
            return $this->select($select)->where(array('activity_code' => $activity_code))->get_all('checkin_user');
        }

        function check_mobile($activity_code,$iphone){
            $where = array(
                'iphone' => $iphone,
                'activity_code' => $activity_code,
            );
            $res = $this->where($where)->get_row('checkin_user');
            return $res['rowCount'];
        }
        
    }
?>