<?php
    /**
    * 
    */
    class iphone extends BaseAction
    {
        function __construct()
        {
            parent::__construct();
            // 数据库加载
            require_once(MODEL_PATH.'iphone.model.php');
            $this->iphoneModel = new iphoneModel();
        }


        // 抽奖界面
        function  lottery(){
            $this->display('lottery.view.html');
        }

        // 抽奖结果显示
        function show_res(){
            $this->display('lottery_result.view.html');
        }

        // 签到页面
        function checkIn(){
            if(_post('activity_code') != ''){
                $data = array(
                    'iphone' => _post('iphone'),
                    'user_name' => _post('user_name'),
                    'company'   => _post('company'),
                    'job'   => _post('job'),
                    'email' => _post('email'),
                    'activity_code' => _post('activity_code'),
                );

                $checkIn_ed = $this->iphoneModel->check_mobile(_post('activity_code'),_post('iphone'));

                if(!$checkIn_ed){
                    $this->iphoneModel->data($data)->insert('checkin_user');
                }

                $this->assign('checkIn_ed',$checkIn_ed);
                $this->display('register_feedback.view.html');

            }else{
                $this->display('checkIn.view.html');
            }
        }


        // 获取抽奖名单
        function get_present_list(){
            $data = $this->iphoneModel->get_present_list(_get('activity_code'));
            echo json_encode($data);
        }

    }

?>