var ldb = {
    // 数据初始化
    dataSource:JSON.parse(window.localStorage.getItem('dataSource')),

    get:function( key ) {
        return window.localStorage.getItem( key ) || ''
    },

    get_array:function(key){
        _res = this.get(key);
        if(_res != ''){
            return _res.split(',');
        }else{
            return false;
        }
        
    },

    // 清空本地数据库
    clear:function(){
        window.localStorage.clear();
    },


    // 展示获奖名单
    display:function(){
        _prize_type = ['first','second','third']; 

        _add_line = "<div class='col-md-2'>{{$user}}</div><div class='col-md-10'>{{$company}}</div>";

        for(var item in _prize_type){
            type = _prize_type[item];
            _prize_list = this.get_array(type);

            if(_prize_list){
                for(var item_a in _prize_list){
                    item_a = _prize_list[item_a];
                    _user_info = this.dataSource[item_a];
                    _output = _add_line.replace("{{$user}}",_user_info['nick']).replace("{{$company}}",_user_info['company']);
                    $('.' + type).append(_output);
                }
            }

        }
    },

    
}

